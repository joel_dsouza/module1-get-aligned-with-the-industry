**What is a Neural Network?**
The basic idea behind a neural network is to simulate (copy in a simplified but reasonably faithful way) lots of densely interconnected brain cells inside a computer so you can get it to learn things, recognize patterns, and make decisions in a humanlike way. The amazing thing about a neural network is that you don't have to program it to learn explicitly: it learns all by itself, just like a brain!

**Working of Neural Network**
A neural network is usually described as having different layers. The first layer is the input layer, it picks up the input signals and passes them to the next layer. The next layer does all kinds of calculations and feature extractions—it’s called the hidden layer. Often, there will be more than one hidden layer. And finally, there’s an output layer, which delivers the result.
[Click Here](https://www.researchgate.net/profile/Paulo_Campos_Souza/publication/331097835/figure/fig2/AS:726236208840705@1550159613046/Artificial-neural-network-of-multiple-layers-and-outputs-31.ppm)

 



**Activation Functions**

Neural network activation functions are a crucial component of deep learning. Activation functions determine the output of a deep learning model, its accuracy, and also the computational efficiency of training a model. The majorly used activation functions include:

**1.	Sigmoid/Logistics**

Sigmoid has a smooth gradient and outputs values between zero and one. For very high or low values of the input parameters, the network can be very slow to reach a prediction, called the vanishing gradient problem.
[Click Here](https://missinglink.ai/wp-content/uploads/2018/11/sigmoidlogisticgraph.png)
 
**2.	TanH/Hyperbolic Tangent**

 It is zero-centered making it easier to model inputs that are strongly negative strongly positive or neutral.
 [Click Here](https://missinglink.ai/wp-content/uploads/2018/11/tanhhyperbolic.png)
 

**3.	ReLu**

ReLu function is highly computationally efficient but is not able to process inputs that approach zero or negative.
[Click Here](https://missinglink.ai/wp-content/uploads/2018/11/relu.png)
 
**4.	Softmax**

Softmax is a special activation function use for output neurons. It normalizes outputs for each class between 0 and 1, and returns the probability that the input belongs to a specific class.
[Click Here](https://missinglink.ai/wp-content/uploads/2018/11/softmax.png)
 



**What is Gradient Descent?**

Gradient descent is a first-order iterative optimization algorithm for finding a local minimum of a differentiable function. 
[Click Here](https://ml-cheatsheet.readthedocs.io/en/latest/_images/gradient_descent_demystified.png)

 

**What is backpropagation?**

Backpropagation is an algorithm commonly used to train neural networks. When the neural network is initialized, weights are set for its individual elements, called neurons. Inputs are loaded, they are passed through the network of neurons, and the network provides an output for each one, given the initial weights. Backpropagation helps to adjust the weights of the neurons so that the result comes closer and closer to the known true result. 
[Click Here](https://missinglink.ai/wp-content/uploads/2018/11/Frame1.png)
