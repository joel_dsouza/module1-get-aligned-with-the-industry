*Convolutional Neural Network*

In neural networks, Convolutional neural network (ConvNets or CNNs) is one of the main categories to do images recognition, images classifications. Objects detections, recognition faces etc., are some of the areas where CNNs are widely used.

*How does CNN Work?*

Technically, deep learning CNN models to train and test, each input image will pass it through a series of convolution layers with filters (Kernals), Pooling, fully connected layers (FC) and apply Softmax function to classify an object with probabilistic values between 0 and 1. The below figure is a complete flow of CNN to process an input image and classifies the objects based on values.

![CNN](https://gitlab.com/ashuEternal/ai-user-training/-/blob/master/summary_images/CNN.jpeg)

*Key Terms :*

- *Kernals* : Kernals are the filters applied while convolution.![Kernals](https://gitlab.com/ashuEternal/ai-user-training/-/blob/master/summary_images/kernel.PNG)
 
- *Stride*  : Stride is the number of pixels shifts over the input matrix.![Stride](https://gitlab.com/ashuEternal/ai-user-training/-/blob/master/summary_images/Stride.PNG)
 
- *Zero padding* : The amount of zeros to put on the image border for efficient filtering of input image.![Padding](https://gitlab.com/ashuEternal/ai-user-training/-/blob/master/summary_images/Padding.PNG)
 
- *Convolution Layer* : The first layer that extracts features from input image.
 
- *ReLU* : ReLU stands for Rectified Linear Unit for a non-linear operation. The output is ƒ(x) = max(0,x).
 
- *Pooling Layer* : Pooling layers section would reduce the number of parameters when the images are too large.![Pooling](https://gitlab.com/ashuEternal/ai-user-training/-/blob/master/summary_images/Pooling.PNG)

- *Fully Connected Layer* : The feature maps are flattened to bring them into one single vector and that becomes Fully      
                              Connected Layer.                                  

- *Flattening* : Flattening layer is used to bring all feature maps matrices into a single vector.